package com.company.graph2;

import java.util.ArrayList;
import java.util.HashMap;


public class Prima {
    public static void main(String[] args) {
        Prima prima = new Prima();
        prima.goPrima(prima.matrix());
    }

    Integer goPrima(int[][] adjacencyMatrix) {
        int result = 0;
        int index = 0;
        ArrayList<Integer> arrayList = new ArrayList();
        arrayList.add(0);
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        int min = 0;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[0][i] != 0){
                min = adjacencyMatrix[0][i];
                break;
            }
        }
        while (arrayList.size() != adjacencyMatrix.length) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i) != null){
                    index = arrayList.get(i);
                }
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (adjacencyMatrix[index][i] < min && adjacencyMatrix[index][i] != 0 && (!arrayList.contains(index)
                            | !arrayList.contains(i))) {
                        min = adjacencyMatrix[index][i];
                        hashMap.put(min, index);
                    }
                }
            }

            result += min;
            arrayList.add(hashMap.get(min));
        }
        return result;
    }

    private int[][] matrix() {
        int[][] matrix =
                {{0, 5, 0, 0, 3, 0},
                        {5, 0, 3, 7, 0, 0},
                        {0, 3, 0, 0, 0, 11},
                        {0, 7, 0, 0, 1, 42},
                        {3, 0, 0, 1, 0, 0},
                        {0, 0, 11, 42, 0, 0}};
        return matrix;
    }

}
