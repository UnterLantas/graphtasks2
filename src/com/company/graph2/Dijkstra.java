package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;


public class Dijkstra {
    ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();

    public static void main(String[] args) {
        Dijkstra dijkstra = new Dijkstra();
        System.out.println(dijkstra.goDijkstra(dijkstra.matrix(), 0));
    }

    HashMap<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        hashMap.put(startIndex, 0);
        arrayDeque.addFirst(startIndex);
        while (!arrayDeque.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[startIndex][i] != 0) {
                    if (!hashMap.containsKey(i)) {
                        hashMap.put(i, adjacencyMatrix[startIndex][i] + hashMap.get(startIndex));
                        if (!arrayDeque.contains(i)) {
                            arrayDeque.addFirst(i);
                        }
                    } else {
                        if (hashMap.get(i) > adjacencyMatrix[startIndex][i] + hashMap.get(startIndex)) {
                            hashMap.remove(i);
                            hashMap.put(i, adjacencyMatrix[startIndex][i] + hashMap.get(startIndex));
                            if (!arrayDeque.contains(i)) {
                                arrayDeque.addFirst(i);
                            }
                        }

                    }


                }

            }
            if (!arrayDeque.isEmpty()) {
                startIndex = arrayDeque.removeLast();
            }
        }

        return hashMap;
    }

    private int[][] matrix() {
        int[][] matrix =
                {{0, 5, 0, 0, 3, 0},
                        {5, 0, 3, 7, 0, 0},
                        {0, 3, 0, 0, 0, 11},
                        {0, 7, 0, 0, 1, 42},
                        {3, 0, 0, 1, 0, 0},
                        {0, 0, 11, 42, 0, 0}};
        return matrix;
    }
}

